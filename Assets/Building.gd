extends Spatial

export(bool) var auto_generate = false
func _ready():
	if(auto_generate):
		rng = RandomNumberGenerator.new()
		rng.randomize()
		generate()

var rng
export(int) var size = 1
export(Color) var color = Color()
var randomized_color
export(float) var MID_HEIGHT = 3
export(float) var TOP_HEIGHT = 3
export(float) var BOTTOM_HEIGHT = 1
export(int) var min_extra_segments = 10
export(int) var max_extra_segments = 30
export(PoolStringArray) var segments_paths
export(bool) var place_collision = true
export(bool) var rotate_top = true
var segments = []
func randomize_height():
	#Duplicate middle segment
	for i in range(rng.randi()%(max_extra_segments-min_extra_segments)+min_extra_segments):
		var mid = duplicate.duplicate()
		add_child(mid)
		segments.insert(duplicate_index, mid)
	#Place middle segments
	for i in range(1, len(segments)-1, 1):
		segments[i].translation.y = i*MID_HEIGHT+BOTTOM_HEIGHT
	#Place top segment
	segments[len(segments)-1].translation.y = segments[len(segments)-2].translation.y+TOP_HEIGHT
	#place collision shapes
	if(place_collision):
		$StaticBody/CollisionShape.shape.extents = Vector3(10, ((segments.size()-1)*MID_HEIGHT)*0.5, 10)
		$StaticBody/CollisionShape.translation.y = ((segments.size()-1)*MID_HEIGHT-BOTTOM_HEIGHT)/2
	$VisibilityNotifier.aabb.position.y = 0
	$VisibilityNotifier.aabb.size.y = ((segments.size()-1)*MID_HEIGHT)
	#rotate top segment
	if(rotate_top):
		segments[len(segments)-1].rotation.y = (rng.randi()%4)*(PI/2)

export(NodePath) var duplicate_path
var duplicate
var duplicate_index
export(bool) var randomize_color = false
export(Vector3) var hsv_shift = Vector3(0.05, 0.1, 0.2)
func generate():
	for i in range(len(segments_paths)):
		if(segments_paths[i] == duplicate_path):
			duplicate_index = i
		segments.append(get_node(segments_paths[i]))
	duplicate = get_node(duplicate_path)
	randomize_height()
	if(randomize_color):
		if(randomized_color == null):
			randomize_color()
		for s in segments:
			if(s.get_active_material(0) is ShaderMaterial):
				s.get_active_material(0).set_shader_param("color", randomized_color)
	setup()

func randomize_color():
	randomized_color = color
	randomized_color.h += rng.randf_range(-hsv_shift.x, hsv_shift.x)
	randomized_color.s += rng.randf_range(-hsv_shift.y, hsv_shift.y)
	randomized_color.v += rng.randf_range(-hsv_shift.z, hsv_shift.z)

export(bool) var random_minimap = false
func get_color():
	if(randomize_color and random_minimap):
		if(randomized_color == null):
			randomize_color()
		return randomized_color
	return color

func setup():
	pass

func _on_VisibilityNotifier_screen_entered():
	visible = true

func _on_VisibilityNotifier_screen_exited():
	visible = false
