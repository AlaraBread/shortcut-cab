extends Spatial

func _on_RigidTimer_timeout():
	queue_free()

var rigid = false
func _on_Area_body_entered(body):
	if(not rigid and body.is_in_group("physics")):
		$RigidBody/CollisionShape.disabled = false
		$RigidBody.mode = RigidBody.MODE_RIGID
		$RigidTimer.start()
