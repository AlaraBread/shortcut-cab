extends MeshInstance

export(bool) var remove_spawners = false

func _ready():
	if(is_instance_valid(GameManager.level)):
		GameManager.level.connect("lights_on", self, "lights_on")
		GameManager.level.connect("lights_off", self, "lights_off")
	if(remove_spawners):
		no_spawners()

func lights_on():
	$Light.visible = true
	$Light2.visible = true
	get_active_material(0).set_shader_param("lights_on", true)

func lights_off():
	$Light.visible = false
	$Light2.visible = false
	get_active_material(0).set_shader_param("lights_on", false)

func no_spawners():
	$Spawner.queue_free()
	$Spawner2.queue_free()
