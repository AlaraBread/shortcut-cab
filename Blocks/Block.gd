extends Spatial

#Represents one city block

func new_building(corner, id=-1):
	if(id==3):
		return preload("res://Assets/Hideouts/PoliceStation.tscn").instance()
	if(id==2):
		return preload("res://Assets/Hideouts/TaxiHideout.tscn").instance()
	if(id==1):
		return preload("res://Assets/Buildings/Pizzeria.tscn").instance()
	if(id==0):
		return preload("res://Assets/Buildings/Garage.tscn").instance()
	if(corner):
		return preload("res://Assets/Buildings/CornerBuilding.tscn").instance()
	if(rng.randi()%10 == 0):
		return preload("res://Assets/Buildings/Skyscraper.tscn").instance()
	return preload("res://Assets/Buildings/Building.tscn").instance()

var special_buildings = []
func add_special_building(id):
	#generate a position that isnt taken
	var done = false
	var position
	while not done:
		done = true
		var r = rng.randi()%4
		if(r==0):
			position = Vector2((rng.randi()%(BLOCK_SIZE-2))+1, 0)
		elif(r==1):
			position = Vector2(BLOCK_SIZE, (rng.randi()%(BLOCK_SIZE-2))+1)
		elif(r==2):
			position = Vector2((rng.randi()%(BLOCK_SIZE-2))+1, BLOCK_SIZE)
		elif(r==3):
			position = Vector2(0, (rng.randi()%(BLOCK_SIZE-2))+1)
		for j in special_buildings:
			if(j[0]==position):
				done = false
				break
	special_buildings.append([position, id])

var minimap
var rng
const BLOCK_SIZE = 5
const BUILDING_SIZE = 20
const MINIMAP_SCALE = 1
var do_not_generate = []
var buildings = []
export(bool) var generate_buildings = true
export(bool) var rotate = false
export(NodePath) var rotate_path
export(Color) var fill_color = Color(0.660156, 0.660156, 0.660156)
export(String) var global_name = ""
func generate():
	if(global_name != ""):
		GameManager.set(global_name, self)
	minimap = Image.new()
	minimap.create((BLOCK_SIZE+1)*BUILDING_SIZE, (BLOCK_SIZE+1)*BUILDING_SIZE, false, Image.FORMAT_RGBA8)
	minimap.fill(fill_color)
	minimap.lock()
	if(rotate):
		var r = rng.randi()%4
		var rot = 0
		if(r == 1):
			rot = PI/2
		elif(r == 2):
			rot = PI
		elif(r == 3):
			rot = (3*PI)/2
		get_node(rotate_path).rotation.y += rot
	if(generate_buildings):
		for x in range(BLOCK_SIZE+1):
			buildings.append([])
			for y in range(BLOCK_SIZE+1):
				#make sure we are on the edge
				if((x == 0 or x == BLOCK_SIZE) or (y == 0 or y == BLOCK_SIZE)):
					var corner = (x==0 and y ==0) or (x==BLOCK_SIZE and y==0) or (x==0 and y==BLOCK_SIZE) or (x==BLOCK_SIZE and y==BLOCK_SIZE)
					var id = -1
					var cur = Vector2(x, y)
					for i in special_buildings:
						if(i[0] == cur):
							id = i[1]
							break
					var b = new_building(corner, id)
					b.rng = rng
					b.generate()
					buildings[x].append(b)
					b.translation = Vector3(x*BUILDING_SIZE, 0, y*BUILDING_SIZE)
					if(b.size != 1):
						do_not_generate.append(Vector2(x, y-1))
						do_not_generate.append(Vector2(x-1, y-1))
						do_not_generate.append(Vector2(x-1, y))
						do_not_generate.append(Vector2(x+1, y+1))
						do_not_generate.append(Vector2(x+1, y-1))
						do_not_generate.append(Vector2(x-1, y+1))
						if(x == 0):
							b.translation.x += (b.size*BUILDING_SIZE)/4
						else:
							b.translation.x -= (b.size*BUILDING_SIZE)/4
						if(y == 0):
							b.translation.z += (b.size*BUILDING_SIZE)/4
						else:
							b.translation.z -= (b.size*BUILDING_SIZE)/4
					if(not corner):
						if(x == 0):
							b.rotate(Vector3.UP, -PI/2)
						elif(x == BLOCK_SIZE):
							b.rotate(Vector3.UP, PI/2)
						elif(y == 0):
							b.rotate(Vector3.UP, PI)
					else:
						if(x == 0 and y == 0):
							b.rotate(Vector3.UP, -PI/2)
						elif(x == BLOCK_SIZE and y == 0):
							b.rotate(Vector3.UP, PI)
						elif(x == 0 and y == BLOCK_SIZE):
							b.rotate(Vector3.UP, 0)
						elif(x == BLOCK_SIZE and y == BLOCK_SIZE):
							b.rotate(Vector3.UP, PI/2)
					add_child(b)
					#set the rect in the minimap
					for m_x in range(0, (BUILDING_SIZE), 1):
						for m_y in range(0, (BUILDING_SIZE), 1):
							minimap.set_pixel(m_x+(x*BUILDING_SIZE), m_y+(y*BUILDING_SIZE), b.get_color())
				else:
					buildings[x].append(null)
	for i in do_not_generate:
		if(i.x >= 0 and i.y >= 0 and i.x < buildings.size() and i.y < buildings[0].size() and buildings[i.x][i.y] != null):
			buildings[i.x][i.y].queue_free()
	minimap.unlock()
	#pedestrian curve
	var offset = (BUILDING_SIZE/2)+3
	var smooth = 1
	var sidewalk = 2
	$PedestrianPath.curve.add_point(Vector3(-offset, 0, -offset+sidewalk), Vector3(), Vector3(0, 0, -smooth))
	$PedestrianPath.curve.add_point(Vector3(-offset+sidewalk, 0, -offset), Vector3(-smooth, 0, 0), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset-sidewalk,0,-offset), Vector3(), Vector3(smooth, 0, 0))
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset,0,-offset+sidewalk), Vector3(0, 0, -smooth), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset,0,BLOCK_SIZE*BUILDING_SIZE+offset-sidewalk), Vector3(), Vector3(0, 0, smooth))
	$PedestrianPath.curve.add_point(Vector3(BLOCK_SIZE*BUILDING_SIZE+offset-sidewalk,0,BLOCK_SIZE*BUILDING_SIZE+offset), Vector3(smooth, 0, 0), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(-offset+sidewalk,0,BLOCK_SIZE*BUILDING_SIZE+offset), Vector3(), Vector3(-smooth, 0, 0))
	$PedestrianPath.curve.add_point(Vector3(-offset,0,BLOCK_SIZE*BUILDING_SIZE+offset-sidewalk), Vector3(0, 0, smooth), Vector3())
	
	$PedestrianPath.curve.add_point(Vector3(-offset, 0, -offset+sidewalk))
	
	#pedestrian area
	$PedestrianArea.translation.x = (BLOCK_SIZE*BUILDING_SIZE)/2
	$PedestrianArea.translation.z = (BLOCK_SIZE*BUILDING_SIZE)/2
	$PedestrianArea/CollisionShape.shape.extents.x = (BLOCK_SIZE*BUILDING_SIZE)
	$PedestrianArea/CollisionShape.shape.extents.z = (BLOCK_SIZE*BUILDING_SIZE)

func place_pedestrian(visual, unit_offset):
	var pedestrian = preload("res://Pedestrians/Pedestrian.tscn").instance()
	visual.rotation = Vector3()
	pedestrian.passenger = visual
	$PedestrianPath.add_car(pedestrian, null, unit_offset)

func place_path_follow(p):
	$PedestrianPath.add_car(p)

func place(object, unit_offset):
	var curve = $PedestrianPath.curve
	var offset = unit_offset*curve.get_baked_length()
	object.global_transform.origin = curve.interpolate_baked(offset)+global_transform.origin
	var bi = curve.bake_interval
	var forward = curve.interpolate_baked(offset+bi)-curve.interpolate_baked(offset)
	forward = forward.normalized()
	var up = curve.interpolate_baked_up_vector(offset)
	var up1 = curve.interpolate_baked_up_vector(offset+bi)
	var axis = up.cross(up1)
	up = up.rotated(axis.normalized(), up.angle_to(up1) * 0.5)
	var sideways = up.cross(forward).normalized()
	up = forward.cross(sideways).normalized()
	object.global_transform.basis.x = sideways
	object.global_transform.basis.y = up
	object.global_transform.basis.z = forward

func get_minimap():
	return minimap
