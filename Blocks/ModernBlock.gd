extends "res://Blocks/Block.gd"

func new_building(corner, id=-1):
	if(id==2):
		return preload("res://Assets/Hideouts/TaxiHideout.tscn").instance()
	if(id==1):
		return preload("res://Assets/Buildings/Pizzeria.tscn").instance()
	if(id==0):
		return preload("res://Assets/Buildings/Garage.tscn").instance()
	if(corner):
		return preload("res://Assets/Buildings/ModernBuildingCorner.tscn").instance()
	if(randi()%15 == 0):
		return preload("res://Assets/Buildings/Skyscraper.tscn").instance()
	return preload("res://Assets/Buildings/ModernBuilding.tscn").instance()
