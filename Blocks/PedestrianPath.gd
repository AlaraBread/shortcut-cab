extends Path

func add_car(c, _spawner=null, unit_offset=null):
	c.cab = GameManager.level.cab
	add_child(c)
	if(unit_offset == null):
		unit_offset = randf()
		while(GameManager.level.cab.global_transform.origin.distance_to(c.global_transform.origin) < 50): # this threshold must be small enough that you cant be in the entire curve and cause an infinite loop
			c.offset = rand_range(0, curve.get_baked_length())
		return
	c.offset = curve.get_baked_length()*unit_offset
