extends VehicleBody

var ENGINE_FORCE = 300 # force that the car moves forward with
export(float) var BRAKE_FORCE = 5000 # force that the car stops with when you brake manually
export(int) var AUTO_BRAKE_FORCE = 100 # force that the car returns to a stop with after you stop pressing throttle
export(float) var steer_speed = 10 # steering speed
export(float) var ROCKET_FORCE = 300000 # force applied by rockets
export(float) var TURNOVER_TORQUE = 180000 # torque applied to the car when steering while upside down
var TRACTION = 3 # default wheel traction
var DRIFT = 1 # friction of back wheels while drifting
var GRAPPLE_STRENGTH = 100000 # strength of grappling hook

var grapple_ray
func _ready():
	grapple_ray = $ViewportContainer/Viewport/Hub/Camera/GrappleRay
	grapple_ray.add_exception(self)
	set_wheel_traction(TRACTION)
	if(GameSaver.get("pizza") == null):
		GameSaver.set("pizza", 0)
	set_pizza(GameSaver.get("pizza"), false)

func is_upside_down():
	return global_transform.basis.y.y < 0

#Checks if the Touching something shape is colliding with anything
func is_touching_something():
	return $TouchingSomethingShape.get_overlapping_bodies().size() > 1

# used to decide how the camera should follow the car's rotation
func is_driving():
	var angle = min(global_transform.basis.y.angle_to(Vector3.UP), (-global_transform.basis.y).angle_to(Vector3.UP))
	var d = angle < 0.5 and is_touching_something()
	if(d):
		if($DrivingTimer.time_left == 0):
			$DrivingTimer.start()
	else:
		is_driving = false
		$DrivingTimer.stop()
	return is_driving

#sets the warp effect strength
func set_warp(w, delta):
	$CanvasLayer/WarpRect.get_material().set_shader_param("warp_scale", 
		lerp($CanvasLayer/WarpRect.get_material().get_shader_param("warp_scale"), (1-w)*0.2, delta*5))
	$CanvasLayer/WarpRect.get_material().set_shader_param("tint_amount", 
		lerp($CanvasLayer/WarpRect.get_material().get_shader_param("tint_amount"), (1-w)*0.25, delta*5))
	$CanvasLayer/WarpRect.get_material().set_shader_param("halo_amount", 
		lerp($CanvasLayer/WarpRect.get_material().get_shader_param("halo_amount"), (1-w)*2, delta*5))

var can_grapple : bool = true

var grappled : bool = false
var grapple_to : Vector3 = Vector3(1, 0, 0)
var grappled_object

func get_point_velocity (point :Vector3)->Vector3:
	return linear_velocity + angular_velocity.cross(point - global_transform.origin)

func _physics_process(delta):
	#calculate speed
	var speed = linear_velocity.project(global_transform.basis.z).length()
	speed *= sign(global_transform.basis.z.dot(linear_velocity))
	
	#sounds
	# drift sound - https://freesound.org/people/audible-edge/sounds/71739/
	if(is_driving() and not is_upside_down() and linear_velocity.length() > 2):
		var slip = get_point_velocity($BR.global_transform.origin).project(global_transform.basis.x).length()
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db, slip*80-40, delta*5)
	else:
		#dont play the if im in the air
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db,-80,delta*5)
	
	#engine sound - https://freesound.org/people/margaridafragata/sounds/508650/
	if(is_driving()):
		$EnginePlayer.pitch_scale = lerp($EnginePlayer.pitch_scale, (abs(speed)*0.15)+1, delta*5)
	else:
		$EnginePlayer.pitch_scale = lerp($EnginePlayer.pitch_scale, 1, delta*5)
	
	#rocket sound - https://freesound.org/people/NickTimesAudio/sounds/494827/
	# movement
	if(Input.is_action_pressed("up")):
		if(speed > 0):
			engine_force = ENGINE_FORCE
		else:
			engine_force = BRAKE_FORCE
	elif(Input.is_action_pressed("down")):
		if(speed < 0):
			engine_force = -ENGINE_FORCE
		else:
			engine_force = -BRAKE_FORCE
	else:
		engine_force = -speed*AUTO_BRAKE_FORCE
	var steer_angle = min((2/(abs(speed)+1)), 0.8)+0.1
	if(Input.is_action_pressed("left")):
		steering = lerp(steering, steering+steer_angle, delta*steer_speed)
		if(is_upside_down()):
			add_torque(Vector3(0, 0, -TURNOVER_TORQUE*delta).rotated(Vector3.UP, rotation.y))
	if(Input.is_action_pressed("right")):
		steering = lerp(steering, steering-steer_angle, delta*steer_speed)
		if(is_upside_down()):
			add_torque(Vector3(0, 0, TURNOVER_TORQUE*delta).rotated(Vector3.UP, rotation.y))
	steering = lerp(steering, 0, delta*steer_speed)
	
	# rocket
	if(has_rocket and Input.is_action_just_pressed("rocket") and can_powerup):
		can_powerup = false
		$PowerupCooldown.start()
		is_rocketing = true
		$RocketPlayer.playing = true # https://freesound.org/people/NickTimesAudio/sounds/494827/
		$RocketTimer.start()
		$Rockets/RocketParticles.emitting = true
		$Rockets/RocketParticles2.emitting = true
		
		$Rockets/ReadyIndicator.visible = false
		$Rockets/ReadyIndicator2.visible = false
	
	#apply rocket force
	if(is_rocketing):
		add_central_force(global_transform.basis.z*(ROCKET_FORCE*delta))
	
	#calculate equipped stuff
	equipped(delta)
	
	#drifting
	if(Input.is_action_pressed("drift")):
		$BR.wheel_friction_slip = DRIFT
		$BL.wheel_friction_slip = DRIFT
		Engine.time_scale = drift_time
		set_warp(drift_time, delta)
	else:
		$BR.wheel_friction_slip = TRACTION
		$BL.wheel_friction_slip = TRACTION
		Engine.time_scale = 1
		set_warp(1, delta)
	#interactions
	var lowest_dist
	var closest
	for i in interactions:
		var d = i.global_transform.origin.distance_to(global_transform.origin)
		if(lowest_dist == null or lowest_dist < d):
			if(not i.allow_during_mission and mission):
				continue
			lowest_dist = d
			closest = i
	if(closest != null):
		$CanvasLayer/InteractLabel.text = closest.get_text()
		$CanvasLayer/InteractLabel.visible = true
		if(Input.is_action_just_pressed("interact")):
			closest.interact()
	else:
		$CanvasLayer/InteractLabel.text = ""
		$CanvasLayer/InteractLabel.visible = false
	
	# targeting
	if(target != null and is_instance_valid(target)):
		$Node/Arrow.visible = true
		$Node/Arrow.look_at(target.global_transform.origin, Vector3.UP)
		$Node/Arrow.global_transform.origin = global_transform.origin+$ArrowPos.translation
	else:
		$Node/Arrow.visible = false
	
	#credits detection
	if(global_transform.origin.y < -1 and not $CanvasLayer/ColorRect/AnimationPlayer.is_playing()):
		$CanvasLayer/ColorRect/AnimationPlayer.play("fade out")
	
	#update minimap
	$CanvasLayer/MinimapContainer/Minimap.set_transform($ViewportContainer/Viewport/Hub.global_transform)
	$CanvasLayer/MinimapContainer/Minimap.set_cab_rotation($ViewportContainer/Viewport/Hub.rotation.y-rotation.y)
	
	#brake lights
	$Body.mesh.surface_get_material(0).set_shader_param("brake_lights", not Input.is_action_pressed("up"))

func _input(event):
	if(Input.is_action_just_pressed("equip next")):
		var changed = false
		while not changed:
			equipped_weapon = (equipped_weapon+1)%NUM_WEAPONS
			if(has_weapons[equipped_weapon]):
				changed = true
		$CanvasLayer/Equipped.set_equipped(equipped_weapon)
	if(Input.is_action_just_pressed("equip prev")):
		var changed = false
		while not changed:
			equipped_weapon = (equipped_weapon-1)
			if(equipped_weapon < 0):
				equipped_weapon = NUM_WEAPONS-1
			if(has_weapons[equipped_weapon]):
				changed = true
		$CanvasLayer/Equipped.set_equipped(equipped_weapon)

const NUM_WEAPONS = 3
enum {NONE, GRAPPLING_HOOK, ROCKET_LAUNCHER}
var has_weapons = {NONE:true, GRAPPLING_HOOK:false, ROCKET_LAUNCHER:false}
var equipped_weapon = NONE
#gets called every frame
func equipped(delta):
	if(Input.is_action_pressed("use equipped")):
		$CanvasLayer/Equipped.visible = false
	grapple_ray.enabled = equipped_weapon != NONE
	if(equipped_weapon == GRAPPLING_HOOK):
		# grappling
		if(grapple_ray.is_colliding() and not grappled):
			$Node/GrappleIndicator.visible = true
			$Node/GrappleIndicator.translation = grapple_ray.get_collision_point()
			if Input.is_action_just_pressed("use equipped") and can_grapple:
				$HookPlayer.play() # hook sound - https://freesound.org/people/16bitstudios/sounds/541975/
				grapple_to = grapple_ray.get_collision_point()
				var o = grapple_ray.get_collider()
				if(o.is_in_group("physics")):
					grappled_object = o
				else:
					grappled_object = null
				grappled = true
				$GrappleRope.visible = grappled
		else:
			$Node/GrappleIndicator.visible = false
		if Input.is_action_just_released("use equipped"):
			grappled = false
			$GrappleRope.visible = grappled
		if grappled:
			if(is_instance_valid(grappled_object)):
				grapple_to = grappled_object.global_transform.origin
			$GrappleRope.look_at(grapple_to, Vector3.UP)
			$GrappleRope.scale.z = $GrappleRope.global_transform.origin.distance_to(grapple_to)
			var pos = (grapple_to - translation).normalized()
			if(is_instance_valid(grappled_object)):
				grappled_object.add_central_force(-pos * (GRAPPLE_STRENGTH * delta))
			add_force(pos * (GRAPPLE_STRENGTH*delta), $GrappleRope.global_transform.origin-global_transform.origin)
	else:
		grappled = false
		$GrappleRope.visible = false
		$Node/GrappleIndicator.visible = false
	if(equipped_weapon == ROCKET_LAUNCHER):
		if(grapple_ray.is_colliding()):
			$Node/GrappleIndicator.visible = can_shoot
			$Node/GrappleIndicator.translation = grapple_ray.get_collision_point()
			var aim_point = grapple_ray.get_collision_point()
			$MissileLauncher/YawRotation.look_at(aim_point, Vector3.UP)
			$MissileLauncher/YawRotation/PitchRotation.rotation.x = $MissileLauncher/YawRotation.rotation.x
			$MissileLauncher/YawRotation/PitchRotation.rotation.z = $MissileLauncher/YawRotation.rotation.z
			$MissileLauncher/YawRotation.rotation.x = 0
			$MissileLauncher/YawRotation.rotation.z = 0
		else:
			$Node/GrappleIndicator.visible = false
		if(Input.is_action_just_pressed("use equipped") and can_shoot):
			var missile = preload("res://Missile.tscn").instance()
			GameManager.level.add_child(missile)
			missile.global_transform.origin = $MissileLauncher.global_transform.origin
			missile.global_transform.basis = $MissileLauncher/YawRotation/PitchRotation.global_transform.basis
			$ShootTimer.start()
			can_shoot = false
			$MissileLauncher/YawRotation/PitchRotation/Missile.visible = can_shoot
			$Node/GrappleIndicator.visible = can_shoot

var can_shoot = true
func _on_ShootTimer_timeout():
	can_shoot = true
	$MissileLauncher/YawRotation/PitchRotation/Missile.visible = can_shoot
	$Node/GrappleIndicator.visible = can_shoot

func _integrate_forces(state):
	#crash sound - me
	for i in range(state.get_contact_count()):
		if(not $CrashPlayer.playing):
			var v = get_point_velocity(state.get_contact_local_position(i)).dot(state.get_contact_local_normal(i))
			if(v > 200):
				$CrashPlayer.unit_db = (v/5)
				$CrashPlayer.playing = true

func cab_visible(v):
	$Body.visible = v
	if(has_rocket):
		$Rockets.visible = v
	else:
		$Rockets.visible = false
	if(drift_time != 1):
		$TimeStopper.visible = v
	else:
		$TimeStopper.visible = false
	if(has_weapons[GRAPPLING_HOOK]):
		$Winch.visible = v
	else:
		$Winch.visible = false
	if(has_weapons[ROCKET_LAUNCHER]):
		$MissileLauncher.visible = v
	else:
		$MissileLauncher.visible = false
	$FR.visible = v
	$FL.visible = v
	$BR.visible = v
	$BL.visible = v

var can_powerup = true
func _on_PowerupCooldown_timeout():
	can_powerup = true
	$Rockets/ReadyIndicator.visible = true
	$Rockets/ReadyIndicator2.visible = true

var is_rocketing = false
func _on_RocketTimer_timeout():
	is_rocketing = false
	$RocketPlayer.playing = false # rocket sound - https://freesound.org/people/NickTimesAudio/sounds/494827/
	$Rockets/RocketParticles.emitting = false
	$Rockets/RocketParticles2.emitting = false

var is_driving = true
func _on_DrivingTimer_timeout():
	is_driving = true

var interactions = []
func add_interaction(i):
	interactions.append(i)

func remove_interaction(i):
	interactions.erase(i)

var target
func set_target(t):
	target = t

var pizza = 0 setget set_pizza, get_pizza

func get_pizza():
	if(pizza_mission):
		return mission_pizza
	return pizza

signal pizza_updated(p)
var pizza_mod = 0
func set_pizza(new, modlabel=true):
	var m
	if(not pizza_mission):
		m = new-pizza
		pizza = new
		$CanvasLayer/PizzaIndicator/PizzaLabel.text = str(pizza)
		$CanvasLayer/GarageMenu/PizzaLabel.text = str(pizza)
		GameSaver.set("pizza", pizza)
		emit_signal("pizza_updated", pizza)
	else:
		m = new-mission_pizza
		mission_pizza = new
		$CanvasLayer/PizzaIndicator/PizzaLabel.text = str(mission_pizza)
		$CanvasLayer/GarageMenu/PizzaLabel.text = str(mission_pizza)
		emit_signal("pizza_updated", mission_pizza)
	if(not modlabel):
		return
	pizza_mod += m
	if(pizza_mod == 0):
		$CanvasLayer/PizzaIndicator/ModLabel.visible = false
		return
	if(pizza_mod > 0):
		$CanvasLayer/PizzaIndicator/ModLabel.modulate = Color(0, 1, 0)
		$CanvasLayer/PizzaIndicator/ModLabel.text = "+"+str(pizza_mod)
	else:
		$CanvasLayer/PizzaIndicator/ModLabel.modulate = Color(1, 0, 0)
		$CanvasLayer/PizzaIndicator/ModLabel.text = str(pizza_mod)
	$CanvasLayer/PizzaIndicator/ModLabel.visible = true
	$PizzaModTimer.start()

var mission = false
var pizza_mission = false
var mission_pizza = 0
func set_pizza_mission(m):
	pizza_mission = m
	if(pizza_mission):
		set_pizza(0, false)
	else:
		set_pizza(pizza, false)
	_on_PizzaModTimer_timeout()

func _on_PizzaModTimer_timeout():
	$CanvasLayer/PizzaIndicator/ModLabel.visible = false
	pizza_mod = 0

func format_seconds(s):
	return "       %02d:%02d:%02d"%[int(s/60),int(s)%60, int(s*60)%(60)]

func set_timer(t):
	$CanvasLayer/Timer.visible = true
	$CanvasLayer/Timer/Label.text = format_seconds(t)

func end_timer(bonus):
	if(bonus != 0):
		$TimeBonusTimer.start()
		$CanvasLayer/Timer/Label.text = "Time Bonus: " + str(bonus)
	else:
		_on_TimeBonusTimer_timeout()

func _on_TimeBonusTimer_timeout():
	$CanvasLayer/Timer.visible = false

func _on_NPCArea_body_entered(body):
	pass

func _on_NPCArea_body_exited(body):
	if(body.is_in_group("car")):
		body.queue_free()
		var cars = 0
		for b in $NPCArea.get_overlapping_bodies():
			if(b.is_in_group("car")):
				cars += 1
		car_quota = MAX_CARS-cars
		var areas = $NPCArea.get_overlapping_areas()
		var s = areas[randi()%len(areas)]
		_on_NPCArea_area_entered(s)
	if(body.is_in_group("pedestrian")):
		body.queue_free()
		var pedestrians = 0
		for b in $NPCArea.get_overlapping_bodies():
			if(b.is_in_group("pedestrian")):
				pedestrians += 1
		pedestrian_quota = MAX_PEDESTRIANS-pedestrians
		var areas = $NPCArea.get_overlapping_areas()
		var s = areas[randi()%len(areas)]
		_on_NPCArea_area_entered(s)

const MAX_CARS = 10
var car_quota = MAX_CARS
const MAX_PEDESTRIANS = 30
var pedestrian_quota = MAX_PEDESTRIANS
func _on_NPCArea_area_entered(area):
	if(area.is_in_group("car spawner") and car_quota > 0):
		car_quota -= 1
		area.spawn()
	if(area.is_in_group("pedestrian spawner") and pedestrian_quota > 0):
		pedestrian_quota -= 1
		area.spawn()

func _on_NPCArea_area_exited(area):
	if(area.is_in_group("car spawner")):
		var cars = 0
		for b in $NPCArea.get_overlapping_bodies():
			if(b.is_in_group("car")):
				cars += 1
		car_quota = MAX_CARS-cars
	if(area.is_in_group("pedestrian spawner")):
		var pedestrians = 0
		for b in $NPCArea.get_overlapping_bodies():
			if(b.is_in_group("pedestrian")):
				pedestrians += 1
		pedestrian_quota = MAX_PEDESTRIANS-pedestrians

func _on_NPCTimer_timeout():
	var areas = $NPCArea.get_overlapping_areas()
	_on_NPCArea_area_entered(areas[randi()%len(areas)])

#Takes number of seconds and displays it on the clock
func set_clock(c):
	$CanvasLayer/Clock/Label.text = "%02d:%02d"%[int(c/60),int(c)%60]

func open_garage():
	linear_velocity = Vector3()
	angular_velocity = Vector3()
	get_tree().paused = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$CanvasLayer/GarageMenu.visible = true

func close_garage():
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$CanvasLayer/GarageMenu.visible = false

#upgrade functions

func set_engine_force(f):
	ENGINE_FORCE = f

func set_wheel_traction(t):
	TRACTION = t
	DRIFT = t/2
	$FL.wheel_friction_slip = TRACTION
	$FR.wheel_friction_slip = TRACTION
	$BL.wheel_friction_slip = TRACTION
	$BR.wheel_friction_slip = TRACTION

func set_grapple_force(f):
	GRAPPLE_STRENGTH = f

var has_rocket = false
func set_rocket_boost(b):
	has_rocket = b
	$Rockets.visible = b

var drift_time = 1
func set_freeze_drift(d):
	drift_time = d
	$TimeStopper.visible = d != 1

func set_wall_pass(w):
	if(w):
		collision_mask = 1+2
	else:
		collision_mask = 1+2+4
	collision_layer = collision_mask

func set_passenger_shield(s):
	GameManager.passenger_shield = s

func set_grapple(g):
	has_weapons[GRAPPLING_HOOK] = g
	$CanvasLayer/Equipped.set_has(has_weapons)
	$Winch.visible = g
	$Node/GrappleIndicator.visible = g

func set_missile(m):
	has_weapons[ROCKET_LAUNCHER] = m
	$CanvasLayer/Equipped.set_has(has_weapons)
	$MissileLauncher.visible = m

func _on_AnimationPlayer_animation_finished(anim_name):
	yield(get_tree(), "idle_frame")
	Engine.time_scale = 1
	Loader.goto_scene("res://Menus/Credits.tscn")

func set_minimap(m):
	$CanvasLayer/MinimapContainer/Minimap.set_minimap(m)

func add_poi(p):
	$CanvasLayer/MinimapContainer/Minimap.add_poi(p)

func remove_poi(p):
	$CanvasLayer/MinimapContainer/Minimap.remove_poi(p)

var passenger
func set_passenger(p):
	passenger = p

func show_dialogue(d):
	$CanvasLayer/Dialogue.display(d)

func set_memo(t):
	if(t==""):
		$CanvasLayer/Memo.visible = false
	else:
		$CanvasLayer/Memo.visible = true
		$CanvasLayer/Memo.set_text(t)

var stars = 0 setget set_stars
func set_stars(s):
	s = clamp(s, 0, 6)
	stars = s
	$CanvasLayer/Wanted.set_stars(ceil(s))

func _on_StarTimer_timeout():
	set_stars(stars-0.1)
