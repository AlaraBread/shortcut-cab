extends Camera

export(NodePath) var target_path
var target

var cab
func _ready():
	cab = get_node("../../../..")
	target = get_node(target_path)

export(float) var speed = 4
func _physics_process(delta):
	#global_transform = global_transform.interpolate_with(target.global_transform, speed*delta)
	cab.cab_visible(get_parent().global_transform.origin.distance_to(global_transform.origin) > 2)
	global_transform = target.global_transform
