extends VehicleBody

export(bool) var randomize_color = true
func _ready():
	var c = Color(1, 0, 0)
	c.v = rand_range(0, 0.5)
	c.s = rand_range(0.5, 1)
	c.h = rand_range(0, 1)
	if(randomize_color):
		$MeshInstance.get_surface_material(0).set_shader_param("color", c)

func is_upside_down():
	return global_transform.basis.y.y < 0

func get_point_velocity (point :Vector3)->Vector3:
	return angular_velocity.cross(point - global_transform.origin)

export(int) var ENGINE_FORCE = 300
export(int) var TAILGATE_FORCE = 500 # force used to avoid being tailgated
const BRAKE_FORCE = 1000000
const STEER_SPEED = 5
func _physics_process(delta):
	#calculate speed
	var speed = linear_velocity.project(global_transform.basis.z).length()
	speed *= sign(global_transform.basis.z.dot(linear_velocity))
	
	#sounds
	# drift sound - https://freesound.org/people/audible-edge/sounds/71739/
	if(linear_velocity.length() > 2):
		var slip = get_point_velocity($BR.global_transform.origin).project(global_transform.basis.x).length()
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db, slip*80-40, delta*5)
	else:
		$DriftPlayer.unit_db = lerp($DriftPlayer.unit_db,-80,delta*5)
	
	#engine sound - https://freesound.org/people/margaridafragata/sounds/508650/
	$EnginePlayer.pitch_scale = lerp($EnginePlayer.pitch_scale, (abs(speed)*0.15)+1, delta*5)
	
	# ai
	ai(delta, speed)

func ai(delta, speed):
	if(not $ForwardRay.is_colliding()):
		if(not $BackRay.is_colliding()):
			engine_force = ENGINE_FORCE
		else:
			engine_force = TAILGATE_FORCE
		$MeshInstance.get_surface_material(0).set_shader_param("brake_lights", false)
	else:
		if(speed > 0):
			engine_force = -BRAKE_FORCE
		else:
			engine_force = -ENGINE_FORCE
		$MeshInstance.get_surface_material(0).set_shader_param("brake_lights", true)
	var steer_angle = clamp((2/(abs(speed)+2))+0.2, 0.1, 0.8)
	if($RightRay.is_colliding()): # turn left
		steering = lerp(steering, steering+steer_angle, delta*STEER_SPEED)
	if($LeftRay.is_colliding()): # turn right
		steering = lerp(steering, steering-steer_angle, delta*STEER_SPEED)
	steering = lerp(steering, 0, delta*STEER_SPEED)

func _exit_tree():
	$DriftPlayer.stop()
	$EnginePlayer.stop()

var player
func _on_CheckInsideTimer_timeout():
	if(global_transform.origin.distance_to(player.global_transform.origin) > 100):
		queue_free()

export(float) var stars = 0.5
func damage():
	queue_free()
	var e = preload("res://Explosion.tscn").instance()
	e.translation = translation
	get_parent().add_child(e)
	GameManager.level.cab.set_stars(GameManager.level.cab.stars+stars)
