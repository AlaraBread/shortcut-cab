extends "res://Cars/Car.gd"

func ai(delta, speed):
	if(GameManager.level.cab.stars > 0 and $CabRay.is_colliding() and $CabRay.get_collider().is_in_group("cab")):
		#turn towards cab
		var angle = Vector2.UP.angle_to(Vector2($CabRay.cast_to.x, $CabRay.cast_to.z))
		if(abs(angle) > PI*1.5 or ($ForwardRay.is_colliding() and not $ForwardRay.get_collider().is_in_group("cab"))):
			#turn around
			angle = (PI/4)*direction*sign(angle)
			engine_force = ENGINE_FORCE*direction
			if(engine_force < 0):
				engine_force *= 3
		else:
			angle *= 0.5
			engine_force = ENGINE_FORCE
		angle = clamp(angle, -PI/3, PI/3)
		steering = lerp(steering, angle, delta*STEER_SPEED)
	else:
		#normal car ai
		if(not $ForwardRay.is_colliding()):
			engine_force = ENGINE_FORCE
		else:
			engine_force = -ENGINE_FORCE
		var steer_angle = clamp((2/(abs(speed)+2))+0.2, 0.1, 0.8)
		if($LeftRay.is_colliding()):
			steering = lerp(steering, steering-steer_angle, delta*STEER_SPEED) # turn right
		if($RightRay.is_colliding()):
			steering = lerp(steering, steering+steer_angle, delta*STEER_SPEED) # turn left
	$CabRay.cast_to = global_transform.xform_inv(GameManager.level.cab.global_transform.origin)
	steering = lerp(steering, 0, delta*STEER_SPEED)
	if(abs(speed)/delta > 50 or $ArrestTimer.time_left != 0):
		$FreeTimer.start()
	for body in $ArrestArea.get_overlapping_bodies():
		if(body.is_in_group("cab")):
			if(linear_velocity.length_squared() < 100):
				if($ArrestTimer.time_left == 0):
					$ArrestTimer.start()
			else:
				$ArrestTimer.stop()

var direction = 1
func _on_TurnaroundTimer_timeout():
	direction *= -1

func _on_FreeTimer_timeout():
	queue_free()

func _on_ArrestArea_body_exited(body):
	if(body.is_in_group("cab")):
		$ArrestTimer.stop()

func _on_ArrestTimer_timeout():
	GameManager.level.arrest_cab()
