extends Control

var labels = []
func _ready():
	labels = $VBoxContainer.get_children()

export(Color) var active_color = Color(1, 1, 1)
export(Color) var inactive_color = Color(0.347656, 0.347656, 0.347656)
func set_equipped(e):
	for i in labels:
		i.modulate = inactive_color
	labels[e].modulate = active_color
	visible = true
	$ShowTimer.start()

func set_has(h):
	for i in range(len(labels)):
		labels[i].visible = h[i]

func _on_ShowTimer_timeout():
	visible = false
