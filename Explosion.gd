extends Area

export(float) var blast_force = 3000
func _ready():
	$CPUParticles.emitting = true
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")
	# explosion sound - https://freesound.org/people/qubodup/sounds/182429/
	for body in get_overlapping_bodies():
		if(body.is_in_group("physics")):
			body.apply_central_impulse((body.global_transform.origin-global_transform.origin).normalized()*blast_force)
		if(body.is_in_group("damageable")):
			body.damage()

func _on_FreeTimer_timeout():
	queue_free()
