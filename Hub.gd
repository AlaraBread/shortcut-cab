extends Spatial

func set_active(a):
	active = a

var mouse = Vector2()
var active = true
var sensitivity = 0.01
const SCROLL_DIST = 0.25
func _input(event):
	if(not active):
		return
	if(event is InputEventMouseMotion):
		if(invert_y):
			event.relative.y *= -1
		mouse = event.relative*sensitivity
	if(Input.is_action_just_pressed("zoom in")):
		set_dist(dist-SCROLL_DIST)
	if(Input.is_action_just_pressed("zoom out")):
		set_dist(dist+SCROLL_DIST)

var initial_dist
var parent
func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	initial_dist = $CameraTarget.translation.z
	$CameraRay.cast_to.z = initial_dist
	dist = initial_dist
	$Camera.global_transform = $CameraTarget.global_transform
	parent = get_node("../../..")
	$CameraRay.add_exception(parent)
	rotation.y = PI

func set_rotate_with_car(r):
	prev_parent_rotation_y = parent.rotation.y
	rotate_with_car = r

var invert_y = false
func set_invert_y(y):
	invert_y = y

var rotate_with_car = true
var prev_parent_rotation_y = 0
func _physics_process(_delta):
	translation = parent.translation
	var is_driving = parent.is_driving()
	if(rotate_with_car):
		if(is_driving):
			rotation.y += parent.rotation.y-prev_parent_rotation_y
		prev_parent_rotation_y = parent.rotation.y
	if(is_driving):
		rotation.x = clamp(rotation.x, (-PI/2.25)+abs(mouse.y), (PI/32)-abs(mouse.y))
	else:
		rotation.x = clamp(rotation.x, (-PI/2.25)+abs(mouse.y), (PI/2)-abs(mouse.y))
	rotate_object_local(Vector3.RIGHT, -mouse.y)
	rotate(Vector3.UP, -mouse.x)
	mouse = Vector2()
	rotation.z = 0
	if($CameraRay.is_colliding()):
		$CameraTarget.translation.z = $CameraRay.get_collision_point().distance_to(global_transform.origin)-camera_buffer
	else:
		$CameraTarget.translation.z = initial_dist-camera_buffer

var camera_buffer = 1
var dist = 0
const MIN_DIST = 2
const MAX_DIST = 10
func set_dist(d):
	d = clamp(d, MIN_DIST, MAX_DIST)
	dist = d
	$CameraTarget.translation.z = d-camera_buffer
	$CameraRay.cast_to.z = d
	initial_dist = d
