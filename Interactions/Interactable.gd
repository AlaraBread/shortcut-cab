extends Area

var cab
var level

export(bool) var allow_during_mission = true

func _ready():
	level = GameManager.level
	setup()

func setup():
	pass

var active = true
export(String) var text = "Press %s to test interaction"
func _on_Interactable_body_entered(body):
	if(body.is_in_group("cab") and active):
		if(body.mission and not allow_during_mission):
			return
		body.add_interaction(self)
		cab = body

func _on_Interactable_body_exited(body):
	if(body.is_in_group("cab")):
		body.remove_interaction(self)
		cab = null

func disable():
	active = false
	if(is_instance_valid(cab)):
		cab.remove_interaction(self)

func interact():
	print("test interact")

func get_text():
	var event = event_from_dict(SettingsSaver.get("keybinds")["interact"])
	return text%event_to_string(event)

func event_from_dict(d):
	if(d["type"] == "key"):
		var e = InputEventKey.new()
		e.set_scancode(d["scancode"])
		return e
	if(d["type"] == "Mouse Button"):
		var e = InputEventMouseButton.new()
		e.set_button_index(d["index"])
		return e

func event_to_string(event):
	if(event is InputEventKey):
		return OS.get_scancode_string(event.get_scancode())
	elif(event is InputEventMouseButton):
		return "mouse button "+str(event.button_index)
	return ""
