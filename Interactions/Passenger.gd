extends "res://Interactions/Interactable.gd"

func interact():
	remove_child(passenger)
	cab.set_passenger(passenger)
	queue_free()
	_on_Interactable_body_exited(cab)
	level.passenger_picked()

func new_passenger():
	if(randi()%2 == 0):
		return preload("res://Assets/Passenger.tscn").instance()
	else:
		return preload("res://Assets/FemalePassenger.tscn").instance()

var passenger
func _ready():
	passenger = new_passenger()
	add_child(passenger)
	passenger.rotation.y += PI/2
	passenger.wait()

func _on_RagdollTimer_timeout():
	queue_free()

export(bool) var stars = 1
var ragdolled = false
func _on_RagdollArea_body_entered(body):
	if(not GameManager.passenger_shield and body.is_in_group("physics") and not ragdolled):
		ragdolled = true
		passenger.ragdoll()
		$POIBody.queue_free()
		$RagdollTimer.start()
		level.place_passenger()
		if(body.is_in_group("cab")):
			body.set_stars(body.stars+stars)
		disable()
