extends "res://Interactions/Interactable.gd"

func setup():
	level.connect("on_call_toggled", self, "on_call_toggled")
	on_call_toggled()

func on_call_toggled():
	if(level.on_call):
		text = "Press %s to go off call"
	else:
		text = "Press %s to go on call"

func interact():
	level.toggle_on_call()
