extends ColorRect

signal reassign(control)
func _on_Reassign_pressed():
	emit_signal("reassign", self)

func event_to_string(event):
	if(event is InputEventKey):
		return OS.get_scancode_string(event.get_scancode())
	elif(event is InputEventMouseButton):
		return "Mouse Button "+str(event.button_index)
	return ""

var binding = ""
var action = ""
func set_action(a):
	action = a[0]
	if(SettingsSaver.get("keybinds") == null):
		SettingsSaver.set("keybinds", {})
	if(not action in SettingsSaver.get("keybinds")):
		SettingsSaver.get("keybinds")[action] = event_to_dict(InputMap.get_action_list(action)[0])
	var event = event_from_dict(SettingsSaver.get("keybinds")[action])
	InputMap.action_erase_events(action)
	InputMap.action_add_event(action, event)
	$ControlLabel.text = a[1]
	binding = event_to_string(event)
	$BindingLabel.text = binding

func not_waiting():
	$BindingLabel.text = binding

func waiting():
	$BindingLabel.text = "Press a key to assign..."

func event_to_dict(e):
	if(e is InputEventKey):
		return {"type":"key", "scancode":e.get_scancode()}
	if(e is InputEventMouseButton):
		return {"type":"mouse button", "index":e.button_index}

func event_from_dict(d):
	if(d["type"] == "key"):
		var e = InputEventKey.new()
		e.set_scancode(d["scancode"])
		return e
	if(d["type"] == "mouse button"):
		var e = InputEventMouseButton.new()
		e.set_button_index(d["index"])
		return e

func assign_button(event):
	SettingsSaver.get("keybinds")[action] = event_to_dict(event)
	InputMap.action_erase_events(action)
	InputMap.action_add_event(action, event)
	binding = event_to_string(event)
	$BindingLabel.text = binding
