extends Spatial

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	MusicManager.play(0)

func _on_StartTimer_timeout():
	$ViewportContainer/Viewport/Path/PathFollow/Camera/AnimationPlayer.play("credits")

func fade_out():
	$CanvasLayer/ColorRect/AnimationPlayer.play("fade out")

func _on_ColorRect_animation_finished(anim_name):
	if(anim_name == "fade out"):
		Loader.goto_scene("res://Menus/MainMenu.tscn")

func _process(delta):
	if(Input.is_action_just_pressed("pause")):
		Loader.goto_scene("res://Menus/MainMenu.tscn")
