extends Control

var upgrades = [
	[
		{"id":0, "name":"Engine Force", "info":"Increased Performance.", "cost":10, "action":["set_engine_force", 500]},
		{"id":1, "name":"Engine Force", "info":"Increased Performance.", "cost":20, "action":["set_engine_force", 700]}
	],
	[
		{"id":2, "name":"Wheel Traction", "info":"Increased Wheel Traction.", "cost":10, "action":["set_wheel_traction", 3]},
		{"id":3, "name":"Wheel Traction", "info":"Increased Wheel Traction.", "cost":20, "action":["set_wheel_traction", 5]},
		{"id":4, "name":"Wheel Traction", "info":"Increased Wheel Traction.", "cost":50, "action":["set_wheel_traction", 10]}
	],
	[
		{"id":11, "name":"Grappling Hook", "info":"Spider car!", "cost":50, "action":["set_grapple", true]},
		{"id":5, "name":"Grappling Hook Tension", "info":"Spider car?", "cost":30, "action":["set_grapple_force", 150000]},
		{"id":6, "name":"Grappling Hook Tension", "info":"Spider car?", "cost":50, "action":["set_grapple_force", 250000]}
	],
	[
		{"id":13, "name":"Missile Launcher", "info":"Annihilate your enemies.", "cost":50, "action":["set_missile", true]}
	],
	[
		{"id":7, "name":"Rocket Boost", "info":"Stap some rocket boosters to your car.", "cost":50, "action":["set_rocket_boost", true]}
	],
	[
		{"id":8, "name":"75% Time Slowing Drift", "info":"Slow time while drifting!", "cost":30, "action":["set_freeze_drift", 0.75]},
		{"id":9, "name":"50% Time Slowing Drift", "info":"Slow time while drifting!", "cost":50, "action":["set_freeze_drift", 0.5]}
	],
	[
		{"id":12, "name":"Passenger Shield", "info":"Prevents you from running over passengers", "cost":50, "action":["set_passenger_shield", true]}
	],
	[
		{"id":10, "name":"Leave the city", "info":"Join your friends.", "cost":100, "action":["set_wall_pass", true]}
	]
]

func _ready():
	if(GameSaver.get("upgrade actions") == null):
		GameSaver.set("upgrade actions", [])
	else:
		var a = GameSaver.get("upgrade actions")
		for i in a:
			get_node("../..").call(i[0], i[1])
	if(GameSaver.get("upgrades") == null):
		GameSaver.set("upgrades", [])
	for i in range(len(upgrades)):
		var u = preload("res://Menus/GarageUpgrade.tscn").instance()
		u.load_list(upgrades[i])
		u.connect("buy", self, "buy_clicked")
		u.connect("hover", self, "upgrade_hover")
		get_node("../..").connect("pizza_updated", u, "pizza_updated")
		$ScrollContainer/VBoxContainer.add_child(u)

func buy_clicked(upgrade):
	var pizza = get_node("../..").pizza
	var cost = upgrade.get_cost()
	if(pizza >= cost):
		GameSaver.get("upgrades").append(upgrade.current["id"])
		var a = upgrade.get_action()
		upgrade.next()
		get_node("../..").pizza -= cost
		get_node("../..").call(a[0], a[1])
		GameSaver.get("upgrade actions").append(a)

func _process(delta):
	if(Input.is_action_just_pressed("pause") and visible):
		_on_Exit_pressed()

func _on_Exit_pressed():
	get_node("../..").close_garage()
	$InfoLabel.text = ""

func upgrade_hover(info):
	$InfoLabel.text = info
