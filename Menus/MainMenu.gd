extends Control

func _ready():
	MusicManager.play(0)
	if(OS.get_name() == "Windows"):
		$WindowsLabel.visible = true
	for i in range(len(GameManager.city_sizes)):
		$CitySize.add_item(str(GameManager.city_sizes[i]), i)
	$CitySize.select(SettingsSaver.get("city size"))

func _on_Start_pressed():
	Loader.goto_scene("res://City.tscn")

func _process(delta):
	$ViewportContainer/Viewport/Camera.rotation.y += delta*0.1
	if(SettingsMenu.get_visible() and Input.is_action_just_pressed("pause")):
		SettingsMenu.pause_pressed()

func _on_Fullscreen_pressed():
	OS.window_fullscreen = not OS.window_fullscreen

func _on_ResetProgress_pressed():
	$ConfirmReset.visible = true

func _on_Reset_Cancel_pressed():
	$ConfirmReset.visible = false

func _on_Reset_Confirm_pressed():
	GameSaver.reset()
	$ConfirmReset.visible = false

func _on_Settings_pressed():
	SettingsMenu.set_visible(true)

func _on_CitySize_item_selected(index):
	SettingsSaver.set("city size", index)
