extends Control

var cab
func _ready():
	cab = get_node("../../..")

const CAMERA_OFFSET = Vector2(30, 30)
func set_transform(t):
	$Viewport/Viewport/Camera2D.position = Vector2(t.origin.x, t.origin.z)+CAMERA_OFFSET
	$Viewport/Viewport/Camera2D.rotation = -t.basis.get_euler().y

func set_cab_rotation(r):
	$Center/Cab.rotation = r+PI

var poi = []
func add_poi(body):
	var p = [body, preload("res://POI.tscn").instance()]
	p[1].set_color(body.color)
	p[1].set_size(body.size)
	$Center.add_child(p[1])
	$Center.move_child(p[1], $Center/Cab.get_index())
	poi.append(p)

func remove_poi(body):
	for i in range(len(poi)):
		if(poi[i][0] == body):
			poi[i][1].queue_free()
			poi.remove(i)
			return

func _process(delta):
	for p in poi:
		var translation = (Vector2(p[0].global_transform.origin.x, p[0].global_transform.origin.z)-($Viewport/Viewport/Camera2D.position-CAMERA_OFFSET))
		translation *= rect_size.x/($Viewport/Viewport.size.x*$Viewport/Viewport/Camera2D.zoom.x)
		translation = translation.rotated(-$Viewport/Viewport/Camera2D.rotation)
		if(p[0].show_offscreen):
			var size = 5*p[0].size
			translation.x = clamp(translation.x, -rect_size.x/2+size, rect_size.x/2-size)
			translation.y = clamp(translation.y, -rect_size.y/2+size, rect_size.y/2-size)
		if(not p[0].show_during_mission):
			p[1].visible = not cab.mission
		p[1].position = translation

func set_minimap(m):
	var texture = ImageTexture.new()
	texture.create_from_image(m)
	$Viewport/Viewport/Sprite.texture = texture
