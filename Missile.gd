extends Area

export(float) var speed = 50
func _physics_process(delta):
	translation += (global_transform.basis.z*(speed*delta))

var can_hit_cab = false
func _on_Missile_body_entered(body):
	if(not can_hit_cab and body.is_in_group("cab")):
		return
	var e = preload("res://Explosion.tscn").instance()
	e.translation = translation
	get_parent().add_child(e)
	queue_free()

func _on_ExitTimer_timeout():
	can_hit_cab = true
