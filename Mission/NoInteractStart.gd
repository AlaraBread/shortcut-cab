extends Area

var mission
func _on_NoInteractStart_body_entered(body):
	if(body.is_in_group("cab")):
		entered()
		queue_free()

func entered():
	GameManager.level.mission_start(mission)
