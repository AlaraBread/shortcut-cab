extends Node

var missions = [
	{
		"type":"goto",
		"interact":true,
		"location":"taxi_hq",
		"start message":{"name":"John", "text":"Welcome to the city!\nI will give you the tour."},
		"end message":{"name":"John", "text":"Here, take some pizza to get yourself started."},
		"memo":"",
		"reward":30,
		"places":[
			{"location":"pizzeria", "memo":"Go to Mama's Pizzeria", "message":{"name":"John", "text":"This is Mama's Pizzeria.\nIt is the city's bank.\nThey also have the best slice in town."}},
			{"location":"garage", "memo":"Go to the garage", "message":{"name":"John", "text":"This is a garage.\nYou can come here to spend your pizza on upgrades."}},
			{"location":"taxi_hq", "memo":"Go to the Taxi HQ", "message":{"name":"John", "text":"This is our headquarters.\nYou can go on call here to earn pizza for yourself."}}
		]
	},
	{
		"type":"pizza",
		"interact":true,
		"location":"taxi_hq",
		"amount":30,
		"start message":{"name":"John", "text":"We are low on pizza right now.\nPlease get us 30 pizza."},
		"end message":{"name":"John", "text":"Thank you for collecting the pizza."},
		"memo":"Earn 30 Pizza.",
		"reward":40
	},
	{
		"type":"kill target",
		"interact":true,
		"location":"taxi_hq",
		"start message":{"name":"John", "text":"A passenger took a ride without paying their fare.\nTake care of them."},
		"end message":{"name":"John", "text":"Good job."},
		"memo":"Kill the highlighted person.",
		"reward":20
	}
]

var pizzeria
var taxi_hq
var police_hq
var park
var garage

var level
var city_sizes = [3, 5, 7, 9]

var passenger_shield = false

func _ready():
	randomize()
