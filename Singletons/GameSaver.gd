extends Node

func get_filename():
	return "user://save.json"

var data = {}
func _ready():
	var f = File.new()
	var e = f.open(get_filename(), File.READ)
	if(e != OK):
		setup()
		return
	var s = f.get_as_text()
	f.close()
	var result = JSON.parse(s)
	if(result.error != OK):
		setup()
		return
	data = result.result
	setup()

func setup():
	for i in [["mission", 0], ["city seed", randi()]]:
		if(GameSaver.get(i[0]) == null):
			GameSaver.set(i[0], i[1])

func save():
	var s = JSON.print(data)
	var f = File.new()
	f.open(get_filename(), File.WRITE)
	f.store_string(s)
	f.close()

func set(key, value):
	data[key] = value

func get(key):
	if(data.has(key)):
		return data[key]
	return null

func reset():
	data = {}
	setup()

func _exit_tree():
	save()
