extends Node

var music = [preload("res://Assets/Music/ShortcutCab.mp3"), preload("res://Assets/Music/MellowCab.mp3")]
var cur_music = -1
func play(s):
	if(cur_music == s):
		return
	cur_music = s
	if(s == -1):
		$MusicPlayer.stop()
		return
	$MusicPlayer.stream = music[s]
	$MusicPlayer.play()
