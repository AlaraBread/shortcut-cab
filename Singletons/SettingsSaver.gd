extends "res://Singletons/GameSaver.gd"

func get_filename():
	return "user://settings.json"

func setup():
	for i in [["fov", 80], ["music", 0.5], ["sfx", 0.5], ["rotate cam", true], ["invert y", false], ["sensitivity", 0.01], ["resolution", 3], ["city size", 1]]:
		if(SettingsSaver.get(i[0]) == null):
			SettingsSaver.set(i[0], i[1])
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("SFX"), linear2db(get("sfx")))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(get("music")))
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Music"), linear2db(get("music")))
