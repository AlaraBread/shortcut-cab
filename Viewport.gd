extends Viewport

func _ready():
	get_tree().connect("screen_resized", self, "resize")
	SettingsMenu.connect("resolution_changed", self, "resize")
	yield(get_tree(), "idle_frame")
	resize()

export(float) var size_mult = 1
export(bool) var square = false
func resize():
	var resolution = SettingsMenu.get_resolution()
	if(square):
		size = Vector2(resolution, resolution)
	else:
		var current = get_tree().get_root().size
		size = Vector2(resolution*(current.x/current.y), resolution)
